# McMule CI lab

This is meant for debugging failed tests (and maybe one day also failed builds).
To use, run
```shell
$ docker run --rm -it yulrich/mcmule-ci-lab
```
You can now use the `mcmule-ci` tool to perform CI actions.
 * `mcmule-ci config` performs the `meson setup` call
 * `mcmule-ci test` runs the CI test single-core
 * `mcmule fetch <branch>` clones the specified branch of McMule, fetches the `build` folder from the most recent CI, and backdates the mtime to avoid unnessary rebuilds.
