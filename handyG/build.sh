mathversion=12.3
mathpath=/usr/local/Wolfram/Mathematica/$mathversion/

rm -rf compiler
cp -R  $mathpath/SystemFiles/Links/MathLink/DeveloperKit/Linux-x86-64/CompilerAdditions/ compiler
cp -R $mathpath/SystemFiles/Libraries/Linux-x86-64/libuuid.a .

docker build -t registry.gitlab.com/mule-tools/handyg/handyg-pre .
docker push registry.gitlab.com/mule-tools/handyg/handyg-pre

docker build -t registry.gitlab.com/mule-tools/handyg/handyg-pre:static -f Dockerfile.static .
docker push registry.gitlab.com/mule-tools/handyg/handyg-pre:static
