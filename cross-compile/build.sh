docker build -t cross:base -f Dockerfile.base .

for i in collier
do
    docker build -t cross:$i-temp -f Dockerfile.$i .
    docker run --name temp-container cross:$i-temp /bin/true
    docker export temp-container | \
        docker import \
        --change "ENV PATH=/usr/osxcross/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin" \
        --change "ENV LD_LIBRARY_PATH=/usr/osxcross/lib" \
        --change "CMD sh" \
        - cross:$i

    docker rm temp-container
    docker rmi cross:$i-temp
    docker images cross:$i*
done

docker tag cross:collier registry.gitlab.com/mule-tools/package-x/cross-compile
docker push registry.gitlab.com/mule-tools/package-x/cross-compile
