mathpath=$(echo 'SetOptions[Streams["stderr"],PageWidth->1000];Write[Streams["stderr"],$InstallationDirectory]' | math  2>&1 > /dev/null)

rm -rf compiler
cp -R  $mathpath/SystemFiles/Links/MathLink/DeveloperKit/Linux-x86-64/CompilerAdditions/ compiler
cp -R $mathpath/SystemFiles/Libraries/Linux-x86-64/libuuid.a .

docker build -t registry.gitlab.com/mule-tools/alphaqed/builder .
docker push registry.gitlab.com/mule-tools/alphaqed/builder .
